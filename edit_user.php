<?php
error_reporting(0);
session_start();
require_once "conexion.php";
require_once "met_crud.php";

if (isset($_SESSION['user_name'])) {
    $nombreUsuario = $_SESSION['user_name'];

        $obj = new metodos();
        $sql= "SELECT * FROM person WHERE user_name='$nombreUsuario'";
        $datos = $obj->mostrarperson($sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/estilos.css">
    <title>Editar Usuario</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous"> 
</head>
<body>
    <?php include('navbar.php'); ?>
    <header>
           <div class="alert alert-success">
           </div>
    </header>
    <div class="container-fluid">
     <div class="row">
     <div class="col-md-2 bg-light d-sm-none d-md-block prim-p">
                    <br>
                    <?php
                            foreach($datos as $key) {
                                $id = $key['id_person'];
                        ?>
                    <img class="col-md-12 img_pp" src="images_user/<?=$key['avatar']; ?>" alt=""><br><hr>
                    <form action="" method="post">
                    <div class="panel col-md-12 bg-light">

                                <strong><?=$key['user_name'];?> </strong><br>
                                <strong><?=$key['name'];?> </strong><br>
                                
                        <?php
                            }
                        ?>
                 </div><hr>
                    </form>
                    <div>
                        <div class="col-md-12">
                        <h6>Amigos</h6>
                        <form action="index.php" class="" method="POST">
                            <input type="text" class="form-control" placeholder="buscar"><br>
                            <div class="scrolls-am">                            
                                <?php $y= 'person';
                                $objs = new metodos();
                                $personas = $objs->mostrarcompany($y); 
                                foreach($personas as $keyp) {
                                ?>     <div class="cont-am">
                                        <div class="col-md-12">
                                            <img class="img-am" src="images_user/<?=$keyp['avatar'];?>" alt="">
                                        </div>    
                                        <div class="cform-control">
                                            <input class="nom-am" type="submit" value="<?=$keyp['name'];?>" name="name">
                                        </div>
                                        </div>                  
                                <?php 
                                }                                
                                ?>  
                            </div>                          
                        </form>
                    </div>
             </div>                    
        </div>
        <div class="col-md-6 table-light d-md-block formx">
          <div class="col-md-12 m-5">
               <h3>Editar información</h3>              
            <form class="form formitic" action="editar.php" method="POST" enctype="multipart/form-data">
              <br>
               <div class="form-group">
               <label for="imagen">Imagen: </label><br>
					<input type="file" name="imagen" size="20"><br>
               </div>
               <input class="col-md-9" type="hidden" name="id" value="<?=$key['id_person'];?>" required><br>
               <label for="">Nombre completo:</label><br>               
               <input class="col-md-9" placeholder="Nombre de Usuario" type="text" name="name" value="<?=$key['name'];?>" required><br>           
                <br><br>
               <button>Modificar</button><br><br>               
            </form>
            <form action="">
                <input type="button" value="Cambiar contraseña" class="camc">
                <input type="button" value="Cambiar Nombre de usuarios" class="camc">
            </form>
            
          </div>
        </div>                
     </div>
    </div>
    

    <?php include('footer.php'); ?>
</body>
</html>
<?php 
    } else {
    header("location: index.php");
   }
?>