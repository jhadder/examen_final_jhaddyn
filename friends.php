<?php
session_start();
require_once "conexion.php";
require_once "met_crud.php";
$nn ="";
if (isset($_SESSION['user_name'])) {
    $nombreUsuario = $_SESSION['user_name'];

        $obj = new metodos();
        $sql= "SELECT * FROM person WHERE user_name='$nombreUsuario'";
        $datos = $obj->mostrarperson($sql);

        if (isset($_POST['buscarx'])) {
            $obj = new metodos();
            $buscar = $_POST['buscarx'];
        
            $filtro = $obj->buscarperson($buscar);
        
            if($obj->rows($filtro) > 0) {
        
            } else {
              $nn = "No se encontró el nombre: $buscar";
            }
            
        } else {
          echo '';
        }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/estilos.css">
    <title>Editar Usuario</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous"> 

    <script>
        $('document').ready(function() {
          $('#buscar').autocomplete({
            source : "ajax.php"
          });
        });
    </script>
</head>
<body>
    <?php include('navbar.php'); ?>
    <header>
           <div class="alert alert-success">
           </div>
    </header>
    <div class="container-fluid">
     <div class="row">
     <div class="col-md-2 bg-light d-sm-none d-md-block prim-p">
                    <br>
                    <?php
                            foreach($datos as $key) {
                                $id = $key['id_person'];
                        ?>
                    <img class="col-md-12 img_pp" src="images_user/<?=$key['avatar']; ?>" alt=""><br><hr>
                    <form action="" method="post">
                    <div class="panel col-md-12 bg-light">

                                <strong><?=$key['user_name'];?> </strong><br>
                                <strong><?=$key['name'];?> </strong><br>
                                
                        <?php
                            }
                        ?>
                 </div><hr>
                    </form>
                    <div>
                        <div class="col-md-12 tab-cs">
                        <h6>Amigos</h6><br>
                        <form action="list_users.php" class="" method="POST">                            
                            <div class="scrolls-am">                            
                                <?php $y= 'person';
                                $objs = new metodos();
                                if (isset($filtro)==true) {
                                    $fbus = $filtro;
                                  } else {
                                      $myrow = $objs->mostrarcompany($y);
                                      $fbus = $myrow;
                                  }
                                    foreach($fbus as $keyp){
                                ?>     <div class="cont-am">
                                        <div class="col-md-12">
                                            <img class="img-am" src="images_user/<?=$keyp['avatar'];?>" alt="">
                                        </div>    
                                        <div class="cform-control">
                                        <input class="nom-am" type="submit" value="<?=$keyp['name'];?>" name="nombre">
                                        </div>
                                        </div>                  
                                <?php 
                                }                                
                                ?>  
                            </div>                          
                        </form>
                    </div>
             </div>                    
        </div>
        <div class="col-md-6 table-light d-md-block formx-fr">
          <div class="col-md-12">
               <h3>Usuarios de Geek´s</h3>
                        <form action="" class="" method="POST">
                        <input id="buscarx" name="buscarx" type="text" class="form-control" placeholder="buscar"><br>
                        </form>
                        <form action="list_users.php" class="" method="POST">                            
                            <div class="scrolls-fr">  
                            <h5><?=$nn;?></h5>                          
                                <?php $y= 'person';
                                $objs = new metodos();
                                if (isset($filtro)==true) {
                                    $fbus = $filtro;
                                  } else {
                                      $myrow = $objs->mostrarcompany($y);
                                      $fbus = $myrow;
                                  }
                                    foreach($fbus as $keyp){
                                ?>     <div class="cont-am">
                                        <div class="col-md-12">
                                            <img class="img-fr" src="images_user/<?=$keyp['avatar'];?>" alt="">
                                            <a href="#"><?=$keyp['user_name'];?></a>
                                        </div>    
                                        <div class="cform-control">                                            
                                            <input class="nom-am-pr" type="submit" value="<?=$keyp['name'];?>" name="nombre">
                                        </div>
                                        </div>                  
                                <?php 
                                }                                
                                ?>  
                            </div>                          
                        </form>
            
            
          </div>
        </div>                
     </div>
    </div>
    

    <?php include('footer.php'); ?>
</body>
</html>
<?php 
    } else {
    header("location: index.php");
   }
?>