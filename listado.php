<?php
    session_start();
    require_once "conexion.php";
    require_once "met_crud.php";
    require_once "met_ed.php";
    require_once "met_reg.php";

    
    if (isset($_SESSION['user_name'])) {
        $nombreUsuario = $_SESSION['user_name'];

            $obj = new metodos();
            $sql= "SELECT * FROM person WHERE user_name='$nombreUsuario'";
            $datos = $obj->mostrarperson($sql);

?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <link rel="stylesheet" href="css/estilos.css">
            <title>Document</title>
            <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
        </head>
        <body>
        <?php include('navbar.php'); ?>
            <header>
                    <div class="alert alert-success">
                    </div>
            </header>
            
        <div class="container-fluid ">
         <div class="row">
                <div class="col-md-2 bg-light d-sm-none d-md-block prim-p">
                    <br>
                    <?php
                            foreach($datos as $key) {
                                $id = $key['id_person'];
                        ?>
                    <img class="col-md-12 img_pp" src="images_user/<?=$key['avatar']; ?>" alt=""><br><hr>
                    <form action="" method="post">
                    <input class="btn-ed" name="editar" type="submit" value="editar">
                    <div class="panel col-md-12 bg-light">

                                <strong><?=$key['user_name'];?> </strong><br>
                                <strong><?=$key['name'];?> </strong><br>
                                
                        <?php
                            }
                        ?>
                 </div><hr>
                    </form>

                    <div>
                        <div class="col-md-12 tab-cs">
                        <h6>Amigos</h6>
                        <form action="list_users.php" class="" method="POST">
                            <input type="text" class="form-control" placeholder="buscar">
                            <div class="scrolls-am">                            
                                <?php $y= 'person';
                                $objs = new metodos();
                                $personas = $objs->mostrarcompany($y); 
                                foreach($personas as $keyp) {
                                ?>     <div class="cont-am">
                                        <div class="col-md-12">
                                            <img class="img-am" src="images_user/<?=$keyp['avatar'];?>" alt="">
                                        </div> 
                                        <div class="cform-control">
                                            <input class="nom-am" type="submit" value="<?=$keyp['name'];?>" name="nombre">
                                        </div>
                                        </div>                  
                                <?php 
                                }                                
                                ?>  
                                <input type="hidden" value="<?php echo $nombreUsuario; ?>" name="userp">
                            </div>                          
                        </form>
                    </div>
             </div>                    
        </div>

        <div class="col-md-6 bg-light scrolls-p">
        <br>
            <div class="panel bg-light pub-p">
             <form action="met_blog.php" method="POST" enctype="multipart/form-data">
               <input class="col-md-9" type="hidden" name="id" value="<?=$key['id_person'];?>" required><br>
               <input class="im-b" type="text" name="txt_blog" placeholder="Que estás pensando?">
               <div class="inp-f">
               <input type="file" name="i_blog" class="in-file">
               </div><br>
               <button class="btn btn-success btn-pub">Publicar</button><br><br> 
             </form>
            </div> <br>

            
                <?php
                    $obj = new metodos();
                    
                    $sql= "SELECT * FROM blog WHERE id_person='$id' ORDER BY id_blog DESC";                    
                    $datos_blog = $obj->mostrarblog($sql);
                    if(isset($datos_blog)==true){
               
                        foreach($datos_blog as $keys) {
                    ?>
                    <div class="pub-c">                    
                        <div>
                            <img class="circulo" src="images_user/<?=$key['avatar']; ?>" alt="">
                            <strong><?=$key['user_name'];?>:</strong><br>
                        </div>             
                        <div>
                            <strong class="txt_pub"><?=$keys['txt_blog'];?> </strong><br>
                        </div><br>
                        <div class="">
                            <img class="col-md-12" src="blog/<?=$keys['image_b']; ?>" alt=""><br>
                        </div><hr>             
                        <form action="">
                            <input type="hidden" value="Editar" class="bg-success btn-b">
                            <input type="hidden" value="Eliminar" class="bg-success btn-b">
                        </form>
                    </div><br><br>
                            
                     <?php
                        }
                    } else {
                       echo "<h2>publica tus sitios favoritos</h2>";
                    }
                    ?>            
            </div>

        <div class="col-md-4 bg-light">
         <br>
           <h3>Visitas frecuentes</h3>
            <div class="col-md-12 bg-light scrolls"><br>
              <?php 
                $objs = new metodos();

                $myrow = $objs->mostrarfavorites($id);
                    foreach($myrow as $key) { 
              ?>
            <form action="" method="POST">
             <div class="row wdls">
              <div class="col-md-5">
              <input type="hidden" name="id_c" value="<?=$key['id_company'];?>">
                 <strong> <?=$key['name'];?> </strong><br><br>
              </div>
               <div class="col-md-4 imgx">
                <img class="col-md-12 float-right cont-p" src="destino/<?=$key['avatar']; ?>">
               </div><hr>
              <div class="col-md-3 btn_ls">
                <input class="btn btn-success form-control inpbtm" name="ver_c" type="submit" value="VER">
               </div>
              </div>
            </form>
               <?php
                    }
               ?>

             </div>
            </div>
           </div>
        </div>


        <?php include('footer.php'); ?>
        </body>
        </html>

<?php 
    } else {
    header("location: index.php");
   }
?>